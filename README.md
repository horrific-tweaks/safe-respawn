# Safe Respawn

[![Available on GitLab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/horrific-tweaks/safe-respawn)
[![Available on Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/mod/safe-respawn)

Normally, hostile Minecraft mobs will only despawn when the player is very far away. This can cause problems if the player encounters a horde of mobs near their spawnpoint, which can trap them in an endless death loop with the same mobs.

This mod changes the entity logic to allow mobs to despawn if there is no living player nearby. This way, when a player dies, they will respawn without any hostile mobs around them (provided there are no other players in the area).

> This is similar to behavior that occurs on PaperMC servers, possibly due to [their "delay-chunk-unloads-by" implementation](https://github.com/PaperMC/Paper/issues/8731).
 
This change *could* break some obscure types of mob farms which make use of nether or end portals, but I've tried to make sure that isn't the case! Please [report an issue](https://gitlab.com/horrific-tweaks/safe-respawn/-/issues) if you find any unintended consequences.

## License

```
    Copyright (C) 2023 James Fenn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
