- Rewritten closest player check to slightly improve performance

  (it's *really* slight - but that does add up in worlds with a lot of entities)
