package me.jfenn.saferespawn.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(MobEntity.class)
public abstract class MobEntityMixin {

    @Shadow
    protected boolean isDisallowedInPeaceful() {
        return false;
    }

    /**
     * This redirects the first "getClosestPlayer()" call when the entity is
     * checking whether it can de-spawn. By default, this does not check
     * if the player is alive - this function passes in an Entity::isAlive
     * predicate.
     * <p>
     * Result: if there is no nearby alive player, the entity should despawn.
     * </p>
     */
    @Redirect(
            at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;getClosestPlayer(Lnet/minecraft/entity/Entity;D)Lnet/minecraft/entity/player/PlayerEntity;"),
            method = "checkDespawn"
    )
    public PlayerEntity checkDespawn(World world, Entity entity, double maxDistance) {
        // Find the closest living & closest dead player
        double livingPlayerDistance = Double.MAX_VALUE;
        PlayerEntity closestLivingPlayer = null;
        double deadPlayerDistance = Double.MAX_VALUE;
        PlayerEntity closestDeadPlayer = null;

        // This is a manual implementation as opposed to using world.getClosestPlayer(),
        // as it is faster (and reduces distance calculations) to obtain both in one loop
        for (PlayerEntity player : world.getPlayers()) {
            // skip players in spectator mode
            if (player.isSpectator()) continue;

            double distanceSquared = player.squaredDistanceTo(entity);
            // if maxDistance is positive and the player is beyond the max distance, skip them.
            // This should never happen in the vanilla implementation, but we want to match
            // its functionality as closely as possible for mod compatibility
            if (maxDistance >= 0 && distanceSquared > maxDistance*maxDistance)
                continue;

            if (player.isAlive()) {
                if (distanceSquared < livingPlayerDistance) {
                    closestLivingPlayer = player;
                    livingPlayerDistance = distanceSquared;
                }
            } else {
                if (distanceSquared < deadPlayerDistance) {
                    closestDeadPlayer = player;
                    deadPlayerDistance = distanceSquared;
                }
            }
        }

        // The normal behavior does not de-spawn mobs if there is no player in the dimension
        // so we need a special case to handle that without affecting mob farms or portal
        // behavior
        // (i.e. if you send a hostile mob through a nether portal, it should still be there
        // when you go to the other side...)
        if (
                // if there is no living player in the dimension
                closestLivingPlayer == null
                // and there is a dead player in the dimension
                && closestDeadPlayer != null
                // and the mob is hostile
                && isDisallowedInPeaceful()
        ) {
            // check if the dead player is within the entity's de-spawn range (usually 128 blocks)
            int despawnRange = entity.getType().getSpawnGroup().getImmediateDespawnRange();

            if (deadPlayerDistance <= despawnRange*despawnRange) {
                // if so, de-spawn the entity immediately
                entity.discard();
            }
        }

        // otherwise, return the closest living player
        // (which will make the normal behavior act like dead players do not exist)
        return closestLivingPlayer;
    }

}
